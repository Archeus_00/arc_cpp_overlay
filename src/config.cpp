#include "arc_cpp/config.hpp"
#include <stdio.h>
#include <string.h>
#include "arc/std/string.h"
#include "arc_cpp/engine.hpp"
#include "arc/std/config.h"
#include "arc/graphics/config.h"
#include "arc/graphics/renderer.h"
#include "arc/files/config.h"
#include "arc/engine/engine.h"
#include "arc/math/config.h"
#include "arc/std/errno.h"

namespace arc {
    Config *config;

    //Config::Config(): mutex(PTHREAD_MUTEX_INITIALIZER){
    Config::Config(){
        ARC_Config_Create(&config);
        ARC_FilesConfig_Init(config);
        ARC_MathConfig_Init(config);
    }

    Config::~Config(){
        ARC_Config_Destroy(config);
        config = nullptr;
    }

    //engine needs to be created before running this
    void Config::initGraphicsTypes(){
        ARC_GraphicsConfig_Init(config, arc::data->renderer);
    }

    void Config::addKey(const char *type, ARC_ConfigKeyRead keyRead, ARC_ConfigKeyDelete keyDelete){
        ARC_Config_AddKeyCString(config, type, strlen(type), keyRead, keyDelete);
    }

    void Config::load(const char *path){
//        pthread_t thread;
//        pthread_create(&thread, NULL, Config::fileIO, (void *)(new FileIOParams(config, &mutex, path, ARC_CONFIG_FILE_IO_LOAD)));
        fileIO(new FileIOParams(config, path, ARC_CONFIG_FILE_IO_LOAD));
    }

    void Config::unload(const char *path){
//        pthread_t thread;
//        pthread_create(&thread, NULL, Config::fileIO, (void *)(new FileIOParams(config, &mutex, path, ARC_CONFIG_FILE_IO_UNLOAD)));
        fileIO(new FileIOParams(config, path, ARC_CONFIG_FILE_IO_UNLOAD));
    }

    void *Config::fileIO(void *data){
        FileIOParams *fdata = (FileIOParams *)data;
        //pthread_mutex_lock(fdata->mutex);

        ARC_String *fileName;
        ARC_String_CreateWithStrlen(&fileName, (char *)fdata->path);
        ARC_Config_FileIO(fdata->config, fileName, fdata->command);
        ARC_String_Destroy(fileName);
        if(arc_errno){
            ARC_DEBUG_LOG(arc_errno, "ARC_Config_FileIO(config, %s, %d);", fdata->path, fdata->command);
        }

        //pthread_mutex_unlock(fdata->mutex);
        delete fdata;

        return NULL;
    }
}

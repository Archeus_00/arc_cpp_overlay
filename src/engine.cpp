#include "arc_cpp/engine.hpp"
#include "arc/std/handler.h"
#include "arc_cpp/keybinds.hpp"
#include "arc/engine/engine.h"
#include "arc/engine/state.h"

namespace arc {
    ARC_EngineData *data;

    void ARC_State_CleanFn(void *state){
        delete (State *)((ARC_State *)state)->data;
    }

    Engine::Engine(ARC_Point windowSize){
        ARC_EngineData_Create(&data, ARC_State_CleanFn, windowSize);
    }

    Engine::~Engine(){
        ARC_EngineData_Destroy(data);
    }
    
    void Engine::addState(State *state, bool replace){
        ARC_State *addState = new ARC_State;

        *addState = {
            .updateFn = state::updateFn,
            .renderFn = state::renderFn,
            .data = state,
        };

        if(replace){
            removeState(0);
        }

        ARC_Handler_Add(data->state, addState);
    }
    
    void Engine::removeState(uint32_t index){
        ARC_Handler_RemoveIndex(data->state, &index);
    }
    
    void Engine::run(){
        ARC_Engine_Run(data);
    }
}
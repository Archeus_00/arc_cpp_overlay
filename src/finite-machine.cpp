#include "arc_cpp/finite-machine.hpp"

namespace arc {
    FiniteMachine::FiniteMachine(){
        currentState = 0;
        lastState = 0;
    }

    FiniteMachine::~FiniteMachine(){
    }

    uint32_t FiniteMachine::newState(){
        lastState++;
        return lastState - 1;
    }

    void FiniteMachine::addEvent(uint32_t state, uint32_t nextState, finiteMachine::EventFn func){
        states[state][nextState] = func;
    }
    
    void FiniteMachine::switchState(uint32_t nextState){
        states[currentState][nextState]();
        currentState = nextState;
    }
}
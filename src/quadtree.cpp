#include "arc_cpp/quadtree.hpp"
#include "arc/math/point.h"
#include "arc_cpp/engine.hpp"
#include "arc/graphics/rectangle.h"
#include <cstdint>
#include <type_traits>

namespace arc {
    QuadTree::QuadTree(ARC_Rect bounds, ARC_Color boundsColor): entity(nullptr), bounds(bounds), boundsColor(boundsColor){}

    QuadTree::~QuadTree(){
        for(unsigned short i = 0; i < 4; i++){
            if(subdivisions[i]){
                delete subdivisions[i];
            }
        }
    }

    void QuadTree::render(){
        ARC_Rect_Render(&bounds, arc::data->renderer, &boundsColor); 

        for(unsigned short i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisions[i]->render();
            }
        }
    }

    void QuadTree::render(ARC_FPoint *offset){
        ARC_Rect tempBounds = { (int32_t)offset->x + bounds.x, (int32_t)offset->y + bounds.y, bounds.w, bounds.h };
        ARC_Rect_Render(&tempBounds, arc::data->renderer, &boundsColor); 

        for(unsigned short i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisions[i]->render(offset);
            }
        }
    }

    bool QuadTree::insert(QuadTreeEntity *entity){
        if(!entityInBounds(entity)){
            return false;
        }

        if(!this->entity){
            this->entity = entity;
            return true;
        }

        subdivide(entity);
        return true;
    }
    
    bool QuadTree::insertAndCreate(ARC_FPoint pos, uint64_t id, void *data){
        QuadTreeEntity *newEntity = new QuadTreeEntity(pos, id, data);
        return insert(newEntity);
    }

    bool QuadTree::remove(QuadTreeEntity *entity){
        return remove(entity->id);
    }
    
    bool QuadTree::remove(uint64_t id){
        if(entity && entity->id == id){
            entity = nullptr;
            return true;
        }

        bool removed = false; 
        uint8_t subdivisionsLen = 0;
        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisionsLen++;
                if(removed){
                    continue;
                }

                removed = subdivisions[i]->remove(id);
                if(!removed){
                    continue;
                }

                delete subdivisions[i];
                subdivisions[i] = nullptr;
                subdivisionsLen--;
            }
        }

        if(subdivisionsLen != 1 || !removed){
            return removed;
        }

        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                entity = subdivisions[i]->getEntity();
                delete subdivisions[i];
                subdivisions[i] = nullptr;
                return true;
            }
        }

        return removed;
    }
    
    bool QuadTree::removeAndDelete(uint64_t id){
        if(entity && entity->id == id){
            delete entity;
            entity = nullptr;
            return true;
        }

        bool removed = false; 
        uint8_t subdivisionsLen = 0;
        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisionsLen++;
                if(removed){
                    continue;
                }

                removed = subdivisions[i]->removeAndDelete(id);
                if(!removed){
                    continue;
                }

                delete subdivisions[i];
                subdivisions[i] = nullptr;
                subdivisionsLen--;
            }
        }

        if(subdivisionsLen != 1 || !removed){
            return removed;
        }

        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                entity = subdivisions[i]->getEntity();
                delete subdivisions[i];
                subdivisions[i] = nullptr;
                return true;
            }
        }

        return removed;
    }

    void QuadTree::query(ARC_Rect *bounds, std::vector<QuadTreeEntity *> &entities){
        if(!ARC_Rect_Intersects(&this->bounds, bounds)){
            return;
        }

        if(containsBounds(bounds)){
            query(entities);
            return;
        }

        if(entity){
            entities.push_back(entity);
        }

        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisions[i]->query(entities);
            }
        }
    }

    void QuadTree::query(std::vector<QuadTreeEntity *> &entities){
        if(entity){
            entities.push_back(entity);
        }

        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                subdivisions[i]->query(entities);
            }
        }
    }

    uint32_t QuadTree::height(){
        uint32_t max = 0, temp = 0;
        for(uint8_t i = 0; i < 4; i++){
            if(subdivisions[i]){
                uint32_t height = subdivisions[i]->height();
                temp = (height)? temp + height : 1;
            }
            max = (temp > max)? temp : max;
            temp = 0;
        }
        return max;
    }

    QuadTreeEntity *QuadTree::getEntity(){
        return entity;
    }

    void QuadTree::subdivide(QuadTreeEntity *entity){
        if((int32_t)entity->pos.x <= (bounds.x + (bounds.w / 2)) && (int32_t)entity->pos.y <= (bounds.y + (bounds.h / 2))){
            if(!subdivisions[0]){
                subdivisions[0] = new QuadTree({ bounds.x, bounds.y, bounds.w / 2, bounds.h / 2 });
            }

            subdivisions[0]->insert(entity);
            return;
        }

        if((int32_t)entity->pos.y <= (bounds.y + (bounds.h / 2))){
            if(!subdivisions[1]){
                subdivisions[1] = new QuadTree({ bounds.x + bounds.w / 2, bounds.y, bounds.w / 2, bounds.h / 2 });
            }

            subdivisions[1]->insert(entity);
            return;
        }

        if((int32_t)entity->pos.x <= (bounds.x + (bounds.w / 2))){
            if(!subdivisions[2]){
                subdivisions[2] = new QuadTree({ bounds.x, bounds.y + bounds.h / 2, bounds.w / 2, bounds.h / 2 });
            }

            subdivisions[2]->insert(entity);
            return;
        }

        if(!subdivisions[3]){
            subdivisions[3] = new QuadTree({ bounds.x + bounds.w / 2, bounds.y + bounds.h / 2, bounds.w / 2, bounds.h / 2 });
        }

        subdivisions[3]->insert(entity);
    }

    bool QuadTree::entityInBounds(QuadTreeEntity *entity){
        return (int32_t)entity->pos.x >= bounds.x && (int32_t)entity->pos.x <= bounds.x + bounds.w &&
               (int32_t)entity->pos.y >= bounds.y && (int32_t)entity->pos.y <= bounds.y + bounds.h;
    }

    bool QuadTree::containsBounds(ARC_Rect *bounds){
        return bounds->x >= this->bounds.x && bounds->x + bounds->w <= this->bounds.x + this->bounds.w &&
               bounds->y >= this->bounds.y && bounds->y + bounds->h <= this->bounds.y + this->bounds.h;
    }
}
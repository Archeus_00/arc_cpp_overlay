#include "arc_cpp/entity/sprite.hpp"
#include "arc/graphics/sprite.h"
#include "arc/math/point.h"
#include "arc_cpp/config.hpp"
#include "arc/engine/engine.h"
#include "arc/math/rectangle.h"
#include <cstdint>

namespace arc {
    Sprite::Sprite(): angle(0.0){
        sprite = nullptr;
    }

    Sprite::Sprite(ARC_Sprite *sprite, uint64_t id, ARC_Point pos, double animationSpeed){
        setId(id);
        this->sprite = sprite;
        ARC_Rect *sbounds = ARC_Sprite_GetBounds(sprite);

        this->pos = { (float)pos.x, (float)pos.y };
        bounds    = { pos.x, pos.y, sbounds->w, sbounds->h };
        origin    = { 0, 0 };
        angle     = 0.0;
        scaleSize = 1.0;

        this->animationSpeed = animationSpeed;
        animationTime = 0.0;

        update();
    }

    Sprite::Sprite(const char *name, uint64_t id, ARC_Point pos, double animationSpeed){
        setId(id);

        sprite = arc::config->get<ARC_Sprite>(name);
        ARC_Rect *sbounds = ARC_Sprite_GetBounds(sprite);

        this->pos = { (float)pos.x, (float)pos.y };
        bounds = { pos.x, pos.y, sbounds->w, sbounds->h };
        origin = { 0, 0 };
        angle = 0.0;
        scaleSize = 1.0;

        this->animationSpeed = animationSpeed;
        animationTime = 0.0;

        update();
    }

    Sprite::~Sprite(){}

    bool Sprite::operator==(Sprite &other){
        return (
            sprite == other.sprite &&
            bounds.x == other.bounds.x && bounds.y == other.bounds.y && bounds.w == other.bounds.w && bounds.h == other.bounds.h &&
            origin.x == other.origin.x && origin.y == other.origin.y 
        );
    }

    void Sprite::update(){
        bounds.x = pos.x - (int32_t)((double)origin.x * scaleSize);
        bounds.y = pos.y - (int32_t)((double)origin.y * scaleSize);

        runAnimation();
    }

    void Sprite::render(){
        ARC_Sprite_RenderRotated(sprite, arc::data->renderer, &bounds, &origin, angle);
    }

    void Sprite::renderAt(ARC_Point *pos){
        ARC_Rect tempBounds = { pos->x, pos->y, bounds.w, bounds.h };
        ARC_Sprite_RenderRotated(sprite, arc::data->renderer, &tempBounds, &origin, angle);
    }

    void Sprite::renderOffset(ARC_FPoint *offset){
        ARC_Rect tempBounds = { (int32_t)offset->x + bounds.x, (int32_t)offset->y + bounds.y, bounds.w, bounds.h };
        ARC_Sprite_RenderRotated(sprite, arc::data->renderer, &tempBounds, &origin, angle);
    }

    void Sprite::runAnimation(){
        animationTime += arc::data->dt;
        if(animationTime >= animationSpeed){
            ARC_Sprite_IterateFrame(sprite);
            animationTime -= animationSpeed;
        }
    }

    void Sprite::scale(double size){
        scaleSize = size;

        ARC_Rect *sbounds = ARC_Sprite_GetBounds(sprite);
        bounds.w = (int32_t)((double)sbounds->w * size);
        bounds.h = (int32_t)((double)sbounds->h * size);
    }

    void Sprite::scaleFromOrigin(double size){
        ARC_Rect *sbounds = ARC_Sprite_GetBounds(sprite);
        bounds.x -= (int32_t)((double)origin.x * size) - ((double)origin.x * scaleSize);
        bounds.y -= (int32_t)((double)origin.y * size) - ((double)origin.y * scaleSize);
        bounds.w =  (int32_t)((double)sbounds->w * size);
        bounds.h =  (int32_t)((double)sbounds->h * size);

        scaleSize = size;

        pos = {
            .x = (float)bounds.x + (float)((double)origin.x * scaleSize),
            .y = (float)bounds.y + (float)((double)origin.y * scaleSize)
        };

    }

    void Sprite::rotate(double angle){
        this->angle += angle;
    }
    
    void Sprite::setRotation(double angle){
        this->angle = angle;
    }

    void Sprite::centerOrigin(){
        ARC_Rect *sbounds = ARC_Sprite_GetBounds(sprite);
        origin.x = (sbounds->w * scaleSize) / 2;
        origin.y = (sbounds->h * scaleSize) / 2;
    }

    void Sprite::setOrigin(ARC_Point pos){
        origin = pos;
    }

    void Sprite::setBounds(ARC_Rect bounds){
        this->bounds = bounds;
        pos = {
            .x = (float)bounds.x + (float)((double)origin.x * scaleSize),
            .y = (float)bounds.y + (float)((double)origin.y * scaleSize)
        };
    }
    
    void Sprite::setSprite(ARC_Sprite *sprite){
        this->sprite = sprite;
    }

    void Sprite::setSprite(const char *name){
        sprite = arc::config->get<ARC_Sprite>(name);
    }
    
    ARC_Rect Sprite::getBounds(){
        return bounds;
    }

    ARC_Point Sprite::getOrigin(){
        return origin;
    }

    ARC_Sprite *Sprite::getSprite(){
        return sprite;
    }

    double Sprite::getScale(){
        return scaleSize;
    }
}
#include "arc_cpp/entity/moveable.hpp"
#include "arc/math/point.h"
#include "arc/math/vector2.h"
#include <math.h>

namespace arc {
    Moveable::Moveable(ARC_FPoint pos, ARC_Vector2 velocity): pos(pos), velocity(velocity){
    }

    Moveable::~Moveable(){
    }

    void Moveable::move(){
        pos.x += velocity.x;
        pos.y += velocity.y;
    }

    void Moveable::move(ARC_Vector2 *velocity){
        this->velocity = *velocity;

        pos.x += velocity->x;
        pos.y += velocity->y;
    }

    void Moveable::moveTo(ARC_FPoint *coord, float speed){
        velocity = {
            .x = coord->x - pos.x,
            .y = coord->y - pos.y
        };

        ARC_Vector2_Normalize(&velocity);
        velocity.x *= speed;
        velocity.y *= speed;

        move();
    }

    ARC_FPoint Moveable::getPos(){
        return pos;
    }

    ARC_Vector2 Moveable::getVelocity(){
        return velocity;
    }

    void Moveable::setPos(ARC_FPoint pos){
        this->pos = pos;
    }

    void Moveable::setVelocity(ARC_Vector2 velocity){
        this->velocity = velocity;
    }
}
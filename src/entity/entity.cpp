#include "arc_cpp/entity/entity.hpp"

namespace arc {
    Entity::Entity(){
    }

    Entity::~Entity(){
    }

    bool Entity::operator==(Entity &other){
        return other.getId() == id;
    }

    uint64_t Entity::getId(){
        return id;
    }

    void Entity::setId(uint64_t id){
        this->id = id;
    }
}
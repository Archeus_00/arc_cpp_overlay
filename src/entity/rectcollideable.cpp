#include "arc/graphics/color.h"
#include "arc/math/rectangle.h"
#include "arc_cpp/entity/rectcollidable.hpp"
#include "arc_cpp/engine.hpp"
#include "arc/graphics/rectangle.h"

namespace arc {
    RectCollidable::RectCollidable(ARC_Rect hitbox): hitbox(hitbox){
    }

    RectCollidable::~RectCollidable(){
    }

    bool RectCollidable::collides(ARC_Rect *hitbox){
        return (bool)ARC_Rect_Intersects(&(this->hitbox), hitbox);
    }

    bool RectCollidable::collides(ARC_Point *hitpoint){
        return (
            (hitbox.x <= hitpoint->x) &&
            (hitbox.y <= hitpoint->y) &&
            (hitbox.x + hitbox.w >= hitpoint->x) &&
            (hitbox.y + hitbox.h >= hitpoint->y)
        );
    }

    ARC_Rect RectCollidable::getHitbox(){
        return hitbox;
    }

    void RectCollidable::setHitbox(ARC_Rect hitbox){
        this->hitbox = hitbox;
    }

    void RectCollidable::renderCollidable(ARC_Color hitboxColor){
        ARC_Rect_Render(&hitbox, arc::data->renderer, &hitboxColor); 
    }

    void RectCollidable::renderCollidable(ARC_FPoint *offset, ARC_Color hitboxColor){
        ARC_Rect tempBounds = { (int32_t)offset->x + hitbox.x, (int32_t)offset->y + hitbox.y, hitbox.w, hitbox.h };
        ARC_Rect_Render(&tempBounds, arc::data->renderer, &hitboxColor); 
    }
}
#include "arc_cpp/keybinds.hpp"
#include "arc_cpp/engine.hpp"
#include "arc/input/keyboard.h"
#include "arc/input/mouse.h"

namespace arc {
    Keybinds keybinds;

    Keybinds::Keybinds(){}

    Keybinds::~Keybinds(){}

    void Keybinds::registerKeybind(KEYBIND binding, INPUT_TYPE inputType, int32_t arcValue){
        keybindings[binding] = std::pair<INPUT_TYPE, int32_t>(inputType, arcValue);
    }

    bool Keybinds::isInputState(KEYBIND binding, INPUT_STATE state){
        switch(keybindings[binding].first){
            case INPUT_TYPE::MOUSE:
                return mouseInput(binding, state);

            case INPUT_TYPE::KEYBOARD:
                return keyInput(binding, state);
        }

        return false;
    }

    bool Keybinds::mouseInput(KEYBIND binding, INPUT_STATE state){
        ARC_MouseState mouseState = ARC_Mouse_GetState(arc::data->mouse, (ARC_MouseButton)keybindings[binding].second);
        switch(state){
            case INPUT_STATE::PRESSED:
                return mouseState == ARC_MOUSE_PRESSED;

            case INPUT_STATE::RELEASED:
                return mouseState == ARC_MOUSE_RELEASED;

            default:
                return mouseState == ARC_MOUSE_NONE;
        }
        return false;
    }

    bool Keybinds::keyInput(KEYBIND binding, INPUT_STATE state){
        ARC_KeyboardState keyState = ARC_Keyboard_GetState(arc::data->keyboard, (ARC_KeyboardKey)keybindings[binding].second);
        switch(state){
            case INPUT_STATE::PRESSED:
                return keyState == ARC_KEY_PRESSED;

            case INPUT_STATE::RELEASED:
                return keyState == ARC_KEY_RELEASED;

            default:
                return keyState == ARC_KEY_NONE;
        }
    }
}
#include "archeus.hpp"

#ifdef __linux__
int main(int argc, char** argv){
#elif _WIN32
int WinMain(int argc, char** argv){
#endif
    arc::config = new arc::Config();

    ARC_Point windowSize = { 1920, 1080 };

#ifdef ARCHEUS_INIT
    ARC_Point *newWindowSize = arc::init();
    if(newWindowSize != nullptr){
        windowSize = *newWindowSize;
    }
#endif

    arc::Engine *engine = new arc::Engine(windowSize);
    arc::config->initGraphicsTypes();

    arc::State *state = arc::initState();
    arc::Engine::addState(state);

    engine->run();

    delete engine;

#ifdef ARCHEUS_DEINIT
    arc::deinit();
#endif

    delete arc::config;

    return 0;
}
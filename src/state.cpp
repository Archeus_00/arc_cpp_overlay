#include "arc_cpp/state.hpp"

namespace arc {
    namespace state {
        void updateFn(void *data){
            ((arc::State*)data)->update();
        }

        void renderFn(void *data){
            ((arc::State*)data)->render();
        }
    }
}
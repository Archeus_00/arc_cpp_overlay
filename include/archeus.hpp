#include "arc_cpp/archeus.hpp"
#include "arc_cpp/config.hpp"
#include "arc_cpp/engine.hpp"
#include "arc_cpp/finite-machine.hpp"
#include "arc_cpp/keybinds.hpp"
#include "arc_cpp/quadtree.hpp"
#include "arc_cpp/state.hpp"

#include "arc_cpp/entity/entity.hpp"
#include "arc_cpp/entity/sprite.hpp"
#include "arc_cpp/entity/renderable.hpp"

// #include <arc/engine/ecs.h>
#include <arc/engine/engine.h>
#include <arc/engine/state.h>

#include <arc/graphics/circle.h>
#include <arc/graphics/color.h>
#include <arc/graphics/config.h>
#include <arc/graphics/line.h>
#include <arc/graphics/obround.h>
#include <arc/graphics/rectangle.h>
#include <arc/graphics/renderer.h>
#include <arc/graphics/sprite.h>
#include <arc/graphics/spritesheet.h>
#include <arc/graphics/text.h>
#include <arc/graphics/window.h>

#include <arc/input/keyboard.h>
#include <arc/input/mouse.h>

#include <arc/math/circle.h>
#include <arc/math/config.h>
#include <arc/math/obround.h>
#include <arc/math/point.h>
#include <arc/math/rectangle.h>
#include <arc/math/vector2.h>
#include <arc/math/vector3.h>

#include <arc/std/array.h>
#include <arc/std/config.h>
#include <arc/std/errno.h>
#include <arc/std/handler.h>
#include <arc/std/io.h>
#include <arc/std/string.h>
#include <arc/std/vector.h>
#include <arc/std/defaults/config.h>
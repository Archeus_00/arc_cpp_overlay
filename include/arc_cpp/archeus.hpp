#pragma once

#include "arc_cpp/state.hpp"
#include "arc/math/point.h"

namespace arc {
    #ifdef ARCHEUS_INIT
    /**
     * @brief a function that runs after config is init but before the engine is created
     *
     * @return ARC_Point of windows size. can be NULL will default to 1920x1080
    */
    extern ARC_Point *init();
    #endif

    #ifdef ARCHEUS_DEINIT
    /**
     * @brief a function that runs before config is destroyed but after the engine is destroyed
    */
    extern void deinit();
    #endif

    /**
     * @brief user defined function to give starting state of the program
     *
     * @return State starting state of the program
    */
    extern State *initState(); 
}
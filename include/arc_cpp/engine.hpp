#pragma once

#include "arc/engine/engine.h"
#include "arc/math/point.h"
#include "arc_cpp/state.hpp"

namespace arc {
    extern ARC_EngineData *data;

    class Engine {
    public:
        Engine(ARC_Point windowSize = { 1920, 1080 });
        ~Engine();

        static void addState(State *state, bool replace = true);
        static void removeState(uint32_t index = 0);

        void run();
    };
}
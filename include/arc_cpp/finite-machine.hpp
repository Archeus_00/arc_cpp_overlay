#pragma once

#include <stdint.h>
#include <map>
#include <vector>

namespace arc {
    namespace finiteMachine {
        typedef void (*EventFn)();

        typedef std::map<uint32_t, EventFn> NodePaths;
    }

    class FiniteMachine {
    public:
        FiniteMachine();
        ~FiniteMachine();

        uint32_t newState();

        void addEvent(uint32_t state, uint32_t nextState, finiteMachine::EventFn func);

        void switchState(uint32_t nextState);

    private:
        std::map<uint32_t, finiteMachine::NodePaths> states;

        uint32_t currentState;
        uint32_t lastState;
    };
}
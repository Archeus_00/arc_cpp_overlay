#pragma once

#include <stdint.h>
#include "arc/std/string.h"
#include "engine.hpp"
#include "arc/std/config.h"
#include "arc/std/errno.h"
//#include <pthread.h>
#include <queue>
#include <stdio.h>

namespace arc {
    class Config {
    public:
        Config();
        ~Config();

        void initGraphicsTypes();

        void addKey(const char *type, ARC_ConfigKeyRead keyRead, ARC_ConfigKeyDelete keyDelete);

        void load  (const char *path);
        void unload(const char *path);

        template <typename Type>
        Type *get(const char *name){
//            pthread_mutex_lock(&mutex);

            Type *val;
            ARC_String *string;
            ARC_String_CreateWithStrlen(&string, (char *)name);
            ARC_Config_Get(config, string, (void **)&val);
            ARC_String_Destroy(string);
            if(arc_errno){
                ARC_DEBUG_LOG(arc_errno, "ARC_Config_Get(config, %s, &val);", name);
                val = nullptr;
            }

//            pthread_mutex_unlock(&mutex);

            return val;
        }

    private:
        struct FileIOParams {
            ARC_Config *config;
//            pthread_mutex_t *mutex;
            const char *path;
            uint8_t command;

            FileIOParams(ARC_Config *config, const char *path, uint8_t command): config(config), path(path), command(command){}
//            FileIOParams(ARC_Config *config, pthread_mutex_t *mutex, const char *path, uint8_t command): config(config), mutex(mutex), path(path), command(command){}
        };

        static void *fileIO(void *data);

//        pthread_mutex_t mutex;

        ARC_Config *config;
        ARC_EngineData *data;
    };

    extern Config *config;
}

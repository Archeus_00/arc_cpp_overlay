#pragma once
#include "arc_cpp/entity/entity.hpp"
#include "arc/math/point.h"
#include <stdint.h>

namespace arc {
    class Renderable : public Entity {
    public:
        Renderable(){}
        virtual ~Renderable(){}

        virtual void render() = 0;
        virtual void renderAt(ARC_Point *pos) = 0;
        virtual void renderOffset(ARC_FPoint *offset) = 0;

        bool operator==(Renderable &other){
            return other.getId() == id;
        }
    };
}
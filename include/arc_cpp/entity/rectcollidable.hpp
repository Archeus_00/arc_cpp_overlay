#pragma once
#include "collideable.hpp"
#include "arc/math/rectangle.h"
#include "arc/math/point.h"
#include "arc/graphics/color.h"

namespace arc {
    class RectCollidable {
    public:
        RectCollidable(ARC_Rect hitbox = { 0, 0, 1, 1 });
        virtual ~RectCollidable();

        virtual bool collides(ARC_Rect *hitbox);
        virtual bool collides(ARC_Point *hitpoint);

        ARC_Rect getHitbox();

        void setHitbox(ARC_Rect hitbox);

        void renderCollidable(ARC_Color hitboxColor = { .r = 255, .g = 0, .b = 0, .a = 255 });
        void renderCollidable(ARC_FPoint *offset, ARC_Color hitboxColor = { .r = 255, .g = 0, .b = 0, .a = 255 });

    protected:
        ARC_Point hitboxPadding;
        ARC_Rect hitbox;
    };
}
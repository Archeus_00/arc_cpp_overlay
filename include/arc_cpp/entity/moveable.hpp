#pragma once

#include "arc/math/point.h"
#include "arc/math/vector2.h"

namespace arc {
    class Moveable {
    public:
        Moveable(ARC_FPoint pos = { 0, 0 }, ARC_Vector2 velocity = { 0.0, 0.0 });
        virtual ~Moveable();

        virtual void move();
        virtual void move(ARC_Vector2 *velocity);
        virtual void moveTo(ARC_FPoint *coord, float speed);

        ARC_FPoint getPos();
        ARC_Vector2 getVelocity();

        void setPos(ARC_FPoint pos);
        void setVelocity(ARC_Vector2 velocity);

    protected:
        ARC_FPoint pos;
        ARC_Vector2 velocity;
    };
}
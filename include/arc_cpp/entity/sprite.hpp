#pragma once
#include "arc/math/vector2.h"
#include "arc_cpp/entity/moveable.hpp"
#include "arc_cpp/entity/rectcollidable.hpp"
#include "arc_cpp/entity/renderable.hpp"
#include "arc/graphics/sprite.h"
#include "arc/math/point.h"
#include "arc/math/rectangle.h"

namespace arc {
    class Sprite : public Renderable, public Moveable, public RectCollidable {
    public:
        Sprite();
        Sprite(ARC_Sprite *sprite, uint64_t id, ARC_Point pos = { 0, 0 }, double animationSpeed = 24.0);
        Sprite(const char *name  , uint64_t id, ARC_Point pos = { 0, 0 }, double animationSpeed = 24.0);
        virtual ~Sprite();

        bool operator==(Sprite &other);

        virtual void update();

        virtual void render();
        virtual void renderAt(ARC_Point *pos);
        virtual void renderOffset(ARC_FPoint *offset);

        void runAnimation();

        void scale(double size);
        void scaleFromOrigin(double size);

        void rotate(double angle);
        void setRotation(double angle);

        void centerOrigin();
        void setOrigin(ARC_Point pos);

        void setBounds(ARC_Rect    bounds);
        void setSprite(ARC_Sprite *sprite);
        void setSprite(const char *name  );

        ARC_Rect  getBounds();
        ARC_Point getOrigin();

        ARC_Sprite *getSprite();

        double getScale();

    protected:
        ARC_Sprite *sprite;
        ARC_Rect bounds;
        ARC_Point origin;

        double angle;
        double scaleSize;
        double animationSpeed;
        double animationTime;
    };
}
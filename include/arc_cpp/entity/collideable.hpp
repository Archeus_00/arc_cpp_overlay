#pragma once
#include "arc/math/rectangle.h"
#include "arc/math/point.h"
#include "arc_cpp/entity/entity.hpp"
#include "arc/graphics/color.h"

namespace arc {
    class Collidable : public Entity {
    public:
        virtual ~Collidable(){}

        virtual bool collides(ARC_Rect  *hitbox  ) = 0;
        virtual bool collides(ARC_Point *hitpoint) = 0;

        //For debugging purposes
        virtual void renderCollidable(ARC_Color hitboxColor){}
        virtual void renderCollidable(ARC_FPoint *offset, ARC_Color hitboxColor){}
    };
}
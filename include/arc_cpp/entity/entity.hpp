#pragma once
#include <stdint.h>

namespace arc {
    class Entity {
    public:
        Entity();
        virtual ~Entity();

        bool operator==(Entity &other);

        uint64_t getId();

        void setId(uint64_t id);

    protected:
        uint64_t id;
    };
}
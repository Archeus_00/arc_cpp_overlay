#pragma once

#include "arc/graphics/color.h"
#include "arc/math/rectangle.h"
#include "arc/math/point.h"
#include <vector>
#include <stdint.h>

namespace arc {
    struct QuadTreeEntity {
        QuadTreeEntity(ARC_FPoint pos, uint64_t id, void *data): pos(pos), id(id), data(data){}

        ARC_FPoint pos;
        uint64_t id;
        void *data;
    };

    class QuadTree {
    public:
        QuadTree(ARC_Rect bounds, ARC_Color boundsColor = { 255, 0, 0, 255 });
        ~QuadTree();

        void render();
        void render(ARC_FPoint *offset);

        bool insert(QuadTreeEntity *entity);
        bool insertAndCreate(ARC_FPoint pos, uint64_t id, void *data);

        bool remove(QuadTreeEntity *entity);
        bool remove(uint64_t id);
        bool removeAndDelete(uint64_t id);

        void query(ARC_Rect *bounds, std::vector<QuadTreeEntity *> &entities);

        void query(std::vector<QuadTreeEntity *> &entities);

        uint32_t height();

        QuadTreeEntity *getEntity();

    private:
        void subdivide(QuadTreeEntity *entity);

        bool entityInBounds(QuadTreeEntity *entity);

        bool containsBounds(ARC_Rect *bounds);

        QuadTree *subdivisions[4] = { nullptr, nullptr, nullptr, nullptr };

        QuadTreeEntity *entity;

        ARC_Rect bounds;
        ARC_Color boundsColor;
    };
}
#pragma once
#include <unordered_map>
#include <utility>
#include <stdint.h>

namespace arc {
    enum class INPUT_STATE {
        PRESSED,
        RELEASED,
        NONE
    };

    //defined in main program
    enum class KEYBIND;

    enum class INPUT_TYPE {
        MOUSE,
        KEYBOARD,
        // CONTROLLER
    };

    class Keybinds {
    public:
        Keybinds();
        ~Keybinds();
        
        void registerKeybind(KEYBIND binding, INPUT_TYPE inputType, int32_t arcValue);

        bool isInputState(KEYBIND binding, INPUT_STATE state);

    private:
        bool mouseInput(KEYBIND binding, INPUT_STATE state);
        bool keyInput  (KEYBIND binding, INPUT_STATE state);
        //Negative is mouse, positive is key
        std::unordered_map<KEYBIND, std::pair<INPUT_TYPE, int32_t>> keybindings;
    };

    extern Keybinds keybinds;
}
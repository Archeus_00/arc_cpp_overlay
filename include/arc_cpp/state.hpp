#pragma once

namespace arc {
    class State {
    public:
        virtual ~State(){}

        virtual void update() = 0;
        virtual void render() = 0;
    };

    namespace state {
        void updateFn(void *data);
        void renderFn(void *data);
    }
}
